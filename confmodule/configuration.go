package configuration

import (
	"encoding/json"
	"flag"
	"os"
)

// Config is a variable that loads config.json to all modules
var Config = loadConfig()

type configuration struct {
	KPCserver struct {
		Port string `json:port`
	}
	RabbitMQServer struct {
		Host     string `json:host`
		Port     string `json:port`
		Username string `json:username`
		Password string `json:password`
	}
	DataBaseServer struct {
		Host     string `json:host`
		DBName   string `json:dbname`
		ScansCol string `json:scanscol`
		SchedCol string `json:schedcol`
		Username string `json:username`
		Password string `json:password`
	}
	ElasticServer struct {
		Host string `json:host`
		Port string `json:port`
	}
}

func loadConfig() configuration {
	var configpath = flag.String("configpath", "./config.json", "path ot config file")
	flag.Parse()
	configfile, _ := os.Open(*configpath)
	defer configfile.Close()
	decoder := json.NewDecoder(configfile)
	config := configuration{}
	err := decoder.Decode(&config)
	if err != nil {
		return configuration{}
	}
	return config
}
