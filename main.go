package main

import (
	"KPC_API/confmodule"
	//"KPC_API/scheduler"
	"KPC_API/server"
	//"time"
)

var config = configuration.Config

func main() {
	server := server.Run()
	/*go func() {
		for {
			var tsks []scheduler.Task
			col, session := mongoSchedConn()

			col.Find(nil).All(&tsks)
			session.Close()
			for _, t := range tsks {
				scheduler.ScheduleTask(t)
			}
			time.Sleep(5 * time.Second)
		}
	}()*/
	server.Logger.Fatal(server.Start(":" + config.KPCserver.Port))
}

// List of funcs for help and serializing (unused)
/*func mongoSchedConn() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial(config.DataBaseServer.Host)
	if err != nil {
		log.Print(err)
	}
	session.SetMode(mgo.Monotonic, true)
	col := session.DB(config.DataBaseServer.DBName).C(config.DataBaseServer.SchedCol)
	return col, session
}*/
