package reporter

import (
	"KPC_API/confmodule"
	"KPC_API/scanner"
	"context"
	"encoding/json"
	"log"

	"github.com/google/uuid"
	"github.com/olivere/elastic"
)

var config = configuration.Config

type ReporterscanData struct {
	scanner.ScanData
}

type ResultData struct {
	IP          string `json:"ip"`
	Ports       []PortData
	OldScanDate int64
	NewScanDate int64
}

type PortData struct {
	Port                int
	Cves                []scanner.CVE
	PortBannerOld       map[string]interface{}
	PortBannerNew       map[string]interface{}
	PortBannerOldSHA256 string
	PortBannerNewSHA256 string
	PortBannerChanged   bool
	StateCVE            string
	PortIsNew           bool
}

// GetScanData returns all results for scanID
func GetScanData(scanid string) []ReporterscanData {
	scanuuid, err := uuid.Parse(scanid)
	scans := []ReporterscanData{}
	if err != nil {
		log.Fatal(err)
	}
	client, ctx := elasticInit()
	searchResult, err := client.Search().
		Index("scans-*").
		Query(elastic.NewQueryStringQuery("ScanID:" + scanuuid.String())).
		Size(10000).
		Pretty(true).
		Do(ctx)
	if err != nil {
		log.Fatal(err)
	}
	for _, hit := range searchResult.Hits.Hits {
		scan := ReporterscanData{}
		err := json.Unmarshal(*hit.Source, &scan)
		if err != nil {
			log.Fatal(err)
		}
		scans = append(scans, scan)
	}
	return scans
}

func (newscan ReporterscanData) ComparePortCVEs(oldscan ReporterscanData, portdata PortData) PortData {
	portdata.Port = newscan.Port
	portdata = newscan.ComparePortBanners(oldscan, portdata)
	portdata.PortIsNew = false
	for _, newcve := range newscan.Cves {
		if isinSlice(newcve, oldscan.Cves) {
			portdata.Cves = append(portdata.Cves, newcve)
			portdata.StateCVE = "current"
		} else if !isinSlice(newcve, oldscan.Cves) {
			portdata.Cves = append(portdata.Cves, newcve)
			portdata.StateCVE = "new"
		}
	}
	for _, oldcve := range oldscan.Cves {
		if !isinSlice(oldcve, newscan.Cves) {
			portdata.Cves = append(portdata.Cves, oldcve)
			portdata.StateCVE = "old"
		}
	}
	// if oldscan.Cves == nil && newscan.Cves == nil {
	// 	portdata.State = "current"
	// }
	return portdata
}

func (newscan ReporterscanData) ComparePortBanners(oldscan ReporterscanData, portdata PortData) PortData {
	if newscan.PortBannerSHA256 != oldscan.PortBannerSHA256 {
		portdata.PortBannerChanged = true
		portdata.PortBannerNew = newscan.PortBanner
		portdata.PortBannerOld = oldscan.PortBanner
		portdata.PortBannerNewSHA256 = newscan.PortBannerSHA256
		portdata.PortBannerOldSHA256 = oldscan.PortBannerSHA256
	} else {
		portdata.PortBannerChanged = false
		portdata.PortBannerNew = newscan.PortBanner
		portdata.PortBannerNewSHA256 = newscan.PortBannerSHA256
	}
	return portdata
}

func (scan ReporterscanData) GetNewPorts() {
	log.Print("New Host: ", scan.IP, ":", scan.Port, " found, with CVEs: ", scan.Cves)
}

func (scan ReporterscanData) GetOldPorts() {
	log.Print("Host: ", scan.IP, ":", scan.Port, " NOT found, with CVEs: ", scan.Cves)
}

// List of funcs for help and serializing
func elasticInit() (*elastic.Client, context.Context) {
	ctx := context.Background()
	client, err := elastic.NewClient(elastic.SetURL("http://"+config.ElasticServer.Host+":"+config.ElasticServer.Port), elastic.SetScheme("http"), elastic.SetSniff(false))
	if err != nil {
		log.Fatal(err)
	}
	info, code, err := client.Ping("http://" + config.ElasticServer.Host + ":" + config.ElasticServer.Port).Do(ctx)
	log.Printf("Elasticsearch returned with code %d and version %s\n", code, info.Version.Number)
	return client, ctx
}

func isinSlice(cve scanner.CVE, cvelist []scanner.CVE) bool {
	for _, v := range cvelist {
		if v == cve {
			return true
		}
	}
	return false
}
