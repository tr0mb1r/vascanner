package scanner

import (
	"KPC_API/confmodule"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"log"
	"net"
	"os/exec"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/araddon/dateparse"
	"github.com/fatih/structs"
	"github.com/google/uuid"
	nmap "github.com/lair-framework/go-nmap"
	"github.com/streadway/amqp"
	fastping "github.com/tatsushid/go-fastping"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ScanData is a struct for parsed scan results
type ScanData struct {
	IP                 string `json:"ip"`
	Port               int    `json:",omitempty"`
	Portstate          string `json:",omitempty"`
	Cpes               []nmap.CPE
	Cves               []CVE
	PortBanner         map[string]interface{} `json:",omitempty"`
	PortBannerSHA256   string                 `json:",omitempty"`
	StartTime          int64                  `json:",omitempty"`
	EndTime            int64                  `json:",omitempty"`
	ScanID             uuid.UUID
	PortProtocol       string  `json:",omitempty"`
	PortOwner          string  `json:",omitempty"`
	PortServiceProduct string  `json:",omitempty"`
	PingRtt            float64 `json:",omitempty"`
	PortRTTL           float32 `json:",omitempty"`
	ScanDate           int64   `json:",omitempty"`
}

// ScanSet is a struct for listing hosts and its open ports
type ScanSet struct {
	host []Hostd
}

// CVE is a struct for CVE data
type CVE struct {
	Cpe   string
	ID    string
	Score string
	Link  string
}

// Hostd is a struct for host and its open ports
type Hostd struct {
	host  string
	ports []string
}

// Scan struct for mongo data
type Scan struct {
	BsonID      bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name        string        `json:"name,omitempty"`
	IPs         []string      `json:"ips,omitempty"`
	PortStart   int           `json:"portstart,omitempty"`
	PortEnd     int           `json:"portend,omitempty"`
	ScanRunning bool          `json:"scanrunning,omitempty"`
	Results     []string      `json:"results,omitempty"`
}

var config = configuration.Config
var wg sync.WaitGroup
var scanid uuid.UUID
var scandate int64
var err error
var out []byte

// ScanStart starts a Scan
func ScanStart(scan Scan, scannameid string) {
	client, ch, queue := rabbitMQInit()
	defer client.Close()
	defer ch.Close()
	debug.SetMaxThreads(30000)
	strrange := strconv.Itoa(scan.PortStart) + "-" + strconv.Itoa(scan.PortEnd)
	ports := parseports(strrange)
	hosts := scan.IPs
	scans := make(chan ScanSet, 65535)
	scanid, err = uuid.NewRandom()
	if err != nil {
		log.Fatal("SCANID NewUUID: ", err)
	}
	scandate = time.Now().Local().Unix()
	checkPort(hosts, ports, scans, ch, client, queue)
	pls := <-scans
	for _, prt := range pls.host {
		data := ScanData{}
		data.ScanID = scanid
		data.ScanDate = scandate
		if prt.ports != nil {
			portlist := strings.Join(prt.ports, ",")
			scanTarget(prt.host, portlist, ch, client, queue)
		} else {
			pingHost(prt.host, ch, client, queue)
		}
	}
	session, err := mgo.Dial(config.DataBaseServer.Host)
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()
	session.SetMode(mgo.Monotonic, true)
	col := session.DB(config.DataBaseServer.DBName).C(config.DataBaseServer.ScansCol)
	targetid := bson.ObjectIdHex(scannameid)
	err = col.Find(bson.M{"_id": targetid}).One(&scan)
	if err != nil {
		log.Fatal(err)
	}
	colQuerier := bson.M{"_id": scan.BsonID}
	change := bson.M{"$set": bson.M{"scanrunning": false}, "$addToSet": bson.M{"results": scanid.String()}}
	err = col.Update(colQuerier, change)
	if err != nil {
		log.Fatal(err)
	}

	// Alert that scanning is finished

}

// parseports for parsing port range
func parseports(p string) []string {
	strports := strings.Split(p, "-")
	startport, err := strconv.Atoi(strports[0])
	if err != nil {
		log.Fatal("PARSEPORTS str to int: ", err)
	}
	endport, err := strconv.Atoi(strports[1])
	if err != nil {
		log.Fatal("PARSEPORTS str to int: ", err)
	}
	var portlst []string
	for i := startport; i <= endport; i++ {
		ps := strconv.Itoa(i)
		portlst = append(portlst, ps)
	}
	return portlst
}

// checkPort checks that port is open
func checkPort(hosts, ports []string, scans chan ScanSet, ch *amqp.Channel, client *amqp.Connection, queue amqp.Queue) {
	scns := ScanSet{}
	for _, host := range hosts {
		hostd := Hostd{}
		hostd.host = host
		wg.Add(len(ports))
		for _, port := range ports {
			go func(port string) {
				defer wg.Done()
				conn, _ := net.DialTimeout("tcp", host+":"+port, 2*time.Second)
				if conn != nil {
					defer conn.Close()
					hostd.ports = append(hostd.ports, port)
				}
			}(port)
		}
		wg.Wait()
		scns.host = append(scns.host, hostd)
	}
	scans <- scns
}

// scanTarget for scanning host
func scanTarget(host, port string, ch *amqp.Channel, client *amqp.Connection, queue amqp.Queue) {
	if host != "empty" {

		// Alert that scan of IP is started

		out, err := exec.Command("nmap", "-oX", "-", host, "-p", port, "-sV", "-Pn", "-T", "4", "--script", "vulners,banner").Output()
		if err != nil {
			log.Fatal("NMAP Run: ", err)
		}

		nmapdata, err := nmap.Parse(out)
		if err != nil {
			log.Fatal("NMAP ParseOutput: ", err)
		}
		hosts := nmapdata.Hosts
		for _, host := range hosts {
			var scrpitsList []CVE
			for _, address := range host.Addresses {
				data := ScanData{}
				data.ScanID = scanid
				data.ScanDate = scandate
				data.IP = address.Addr
				data.Cves = nil
				tstart, err := host.StartTime.MarshalJSON()
				if err != nil {
					log.Print(err)
				}
				tend, err := host.EndTime.MarshalJSON()
				if err != nil {
					log.Print(err)
				}
				unixstart, err := dateparse.ParseLocal(string(tstart[1 : len(tstart)-1]))
				if err != nil {
					log.Print(err)
				}
				unixend, err := dateparse.ParseLocal(string(tend[1 : len(tend)-1]))
				if err != nil {
					log.Print(err)
				}
				data.StartTime = unixstart.Unix()
				data.EndTime = unixend.Unix()
				for _, port := range host.Ports {
					scrpitsList = nil
					bannermap := make(map[string]interface{})
					data.PortBanner = nil
					data.Port = port.PortId
					data.Portstate = port.State.State
					data.Cpes = port.Service.CPEs
					data.PortOwner = port.Owner.Name
					data.PortServiceProduct = port.Service.Product
					data.PortProtocol = port.Protocol
					data.PortRTTL = port.State.ReasonTTL
					for _, script := range port.Scripts {
						if script.Output != "" {
							if script.Id == "vulners" {
								somedata := parseVulners(script)
								if somedata != nil {
									scrpitsList = somedata
								}
							} else if script.Id == "fingerprint-strings" {

							} else {
								bannermap[script.Id] = script.Output
							}
						}
					}
					data.Cves = scrpitsList
					if len(bannermap) != 0 {
						data.PortBanner = bannermap
						bannersha := sha256.New()
						jsonbanner, err := json.Marshal(data.PortBanner)
						if err != nil {
							log.Fatal(err)
						}
						bannersha.Write(jsonbanner)
						data.PortBannerSHA256 = hex.EncodeToString(bannersha.Sum(nil))
					}
					mappeddata := structs.Map(data)
					jsonString, err := json.Marshal(mappeddata)
					if err != nil {
						log.Fatal("NMAP Struct to Map: ", err)
					}
					rabbitMQConn(jsonString, ch, client, queue)

					// Alert that scan of IP finished

				}
			}
		}
	} else {
		log.Fatal("NMAP: Nothing to scan.")
	}
}

// parseVulners is for parsing Vulners result
func parseVulners(script nmap.Script) []CVE {
	var cvedata []CVE
	var cveitem CVE
	parsedstrings := strings.Split(script.Output, "\n\t")
	for _, parsedstring := range parsedstrings[1:len(parsedstrings)] {
		someparse := strings.Split(parsedstring, "\t\t")
		cveitem.ID = someparse[0]
		cveitem.Score = someparse[1]
		cveitem.Link = someparse[2]
		cpeheader := strings.Replace(parsedstrings[0], "\n  ", "", -1)
		cpeheader = strings.Replace(cpeheader, " ", "", -1)
		cveitem.Cpe = cpeheader
		if cveitem != (CVE{}) {
			cvedata = append(cvedata, cveitem)
		}
	}
	return cvedata
}

// pingHost for getting ttl of host
func pingHost(host string, ch *amqp.Channel, client *amqp.Connection, queue amqp.Queue) {
	p := fastping.NewPinger()
	ra, err := net.ResolveIPAddr("ip4:icmp", host)
	if err != nil {
		log.Fatal("PING: ", err)
	}
	p.AddIPAddr(ra)
	p.OnRecv = func(addr *net.IPAddr, rtt time.Duration) {
		data := ScanData{}
		data.ScanID = scanid
		data.ScanDate = scandate
		data.PingRtt = 1000 * rtt.Seconds()
		data.IP = host
		mappeddata := structs.Map(data)
		jsonString, err := json.Marshal(mappeddata)
		if err != nil {
			log.Fatal("NMAP Struct to Map: ", err)
		}
		rabbitMQConn(jsonString, ch, client, queue)
		//fmt.Println(string(jsonString))
	}
	p.OnIdle = func() {
	}
	err = p.Run()
	if err != nil {
		log.Fatal("PING: ", err)
	}
}

// rabbitMQInit initiates connection to RabbitMQ server
func rabbitMQInit() (*amqp.Connection, *amqp.Channel, amqp.Queue) {
	var rabbiturl string
	rabbiturl = "amqp://" + config.RabbitMQServer.Username + ":" + config.RabbitMQServer.Password + "@" + config.RabbitMQServer.Host + ":" + config.RabbitMQServer.Port + "/"
	client, err := amqp.Dial(rabbiturl)
	if err != nil {
		log.Fatal("RABBITMQ Client: ", err)
	}
	ch, err := client.Channel()
	if err != nil {
		log.Fatal("RABBITMQ Channel: ", err)
	}
	queue, err := ch.QueueDeclare(
		"scans",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatal("RABBITMQ Queue: ", err)
	}
	return client, ch, queue
}

// rabbitMQConn sending data to RabbitMQ server
func rabbitMQConn(data []byte, ch *amqp.Channel, client *amqp.Connection, queue amqp.Queue) {
	body := []byte{}
	body = append(body, data...)
	err := ch.Publish(
		"scans",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        data,
		})
	if err != nil {
		log.Fatal("RABBITMQ Connection: ", err)
	}
}
