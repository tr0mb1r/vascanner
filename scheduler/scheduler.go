package scheduler

import (
	"KPC_API/scanner"
	"log"
	"sync"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	// StatePending - initial state of a task
	StatePending = "PENDING"
	// StateReceived - when task is received by a worker
	//StateReceived = "RECEIVED"
	// StateStarted - when the worker starts processing the task
	StateStarted = "STARTED"
	// StateRetry - when failed task has been scheduled for retry
	//StateRetry = "RETRY"
	// StateSuccess - when the task is processed successfully
	//StateSuccess = "SUCCESS"
	// StateFailure - when processing of the task fails
	//StateFailure = "FAILURE"
)

type Task struct {
	TaskID   bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Created  time.Time     `json:"created,omitempty"`
	LastRun  time.Time     `json:"lastrun,omitempty"`
	NextRun  time.Time     `json:"nextrun,omitempty"`
	Duration time.Duration `json:"duration,omitempty"`
	ScanID   string        `json:"scanid,omitempty"`
	Status   string        `json:"status,omitempty"`
}

func (task Task) New(scanid string, duration string) (Task, error) {
	task.TaskID = bson.NewObjectId()
	task.ScanID = scanid
	task.Created = time.Now().UTC()
	task.LastRun = time.Time{}
	timeduration, _ := time.ParseDuration(duration)
	task.Duration = timeduration
	task.NextRun = task.Created.Add(task.Duration)
	task.Status = StatePending
	col, session := mongoSchedConn()
	defer session.Close()
	err := col.Insert(task)
	if err != nil {
		return Task{}, err
	}
	return task, nil
}

var wg sync.WaitGroup

func (task Task) Run() error {
	var scan scanner.Scan
	col, session := mongoScanConn()
	defer session.Close()
	err := col.Find(bson.M{"_id": bson.ObjectIdHex(task.ScanID)}).One(&scan)
	if err != nil {
		return err
	}
	go func() {
		col, session := mongoSchedConn()
		defer session.Close()
		col.Update(bson.M{"_id": task.TaskID}, bson.M{"$set": bson.M{"nextrun": task.NextRun.Add(task.Duration), "lastrun": task.NextRun, "status": StateStarted}})
		wg.Add(1)
		go func() {
			//log.Print("started..." + scan.Name)
			go scanner.ScanStart(scan, task.ScanID)
			wg.Wait()
		}()
		col.Update(bson.M{"_id": task.TaskID}, bson.M{"$set": bson.M{"status": StatePending}})
	}()
	return nil
}

func ScheduleTask(tsk Task) {
	if time.Now().UTC().Unix() >= tsk.NextRun.UTC().Unix() && tsk.Status == StatePending {
		tsk.Run()
	}
}

// List of funcs for help and serializing
func mongoSchedConn() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("localhost")
	if err != nil {
		log.Print(err)
	}
	session.SetMode(mgo.Monotonic, true)
	col := session.DB("revel").C("scheduler")
	return col, session
}
func mongoScanConn() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("localhost")
	if err != nil {
		log.Print(err)
	}
	session.SetMode(mgo.Monotonic, true)
	col := session.DB("revel").C("scanlist")
	return col, session
}

/*func inTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}*/
