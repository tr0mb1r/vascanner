package server

import (
	"KPC_API/confmodule"
	"KPC_API/reporter"
	"KPC_API/scanner"
	"context"
	"encoding/json"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"

	"github.com/google/uuid"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/olivere/elastic"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var config = configuration.Config

// NewScan for POST requests
type NewScan struct {
	Name        string `json:"name" xml:"name" form:"name" query:"name"`
	IPs         string `json:"ips" xml:"ips" form:"ips" query:"ips"`
	PortStart   int    `json:"portstart" xml:"portstart" form:"portstart" query:"portstart"`
	PortEnd     int    `json:"portend" xml:"portend" form:"portend" query:"portend"`
	ScanRunning bool   `json:"scanrunning" xml:"scanrunning" form:"scanrunning" query:"scanrunning"`
}

// Run starts Echo server
func Run() *echo.Echo {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	/*e.Use(middleware.CSRFWithConfig(middleware.CSRFConfig{
		TokenLookup: "header:X-CSRF-TOKEN",
	}))
	e.Use(middleware.Secure())*/

	// Routes
	e.GET("/", index)
	e.POST("/newscan", newScan)
	e.GET("/scanlist", getScans)
	e.GET("/scanlist/:id", getScan)
	e.GET("/results/:id/:resultid", getScanResult)
	e.PUT("/scanlist/:id", updateScan)
	e.DELETE("/scanlist/:id", deleteScan)
	e.GET("/runscan/:id", runScan)
	e.GET("/reporter/:os/:ns", reportScan)
	//e.POST("/schedulescan/:id", scheduleScan)
	return e
}

// Handlers
func index(c echo.Context) error {
	return c.NoContent(http.StatusOK)
}

func getScans(c echo.Context) error {
	var scans []scanner.Scan
	col, session := mongoConn()
	defer session.Close()
	col.Find(nil).All(&scans)
	return c.JSONPretty(http.StatusOK, scans, "    ")
}

func getScan(c echo.Context) error {
	var scan scanner.Scan
	scanid := c.Param("id")
	col, session := mongoConn()
	defer session.Close()
	col.Find(bson.M{"_id": bson.ObjectIdHex(scanid)}).One(&scan)
	return c.JSONPretty(http.StatusOK, scan, "    ")
}

func deleteScan(c echo.Context) error {
	scanid := c.Param("id")
	col, session := mongoConn()
	defer session.Close()
	err := col.Remove(bson.M{"_id": bson.ObjectIdHex(scanid)})
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	return c.NoContent(http.StatusOK)
}

func updateScan(c echo.Context) error {
	scanid := c.Param("id")
	col, session := mongoConn()
	defer session.Close()
	var updscan scanner.Scan
	scanrequest := new(NewScan)
	if err := c.Bind(scanrequest); err != nil {
		return err
	}
	updscan.Name = scanrequest.Name
	updscan.PortStart = scanrequest.PortStart
	updscan.PortEnd = scanrequest.PortEnd
	wholeiplist := strings.Split(scanrequest.IPs, ",")
	/*for _, ip := range wholeiplist {
		if !stringInSlice(ip, updscan.IPs) {
			updscan.IPs = append(updscan.IPs, ip)
		}
	}*/
	for _, ip := range wholeiplist {
		if strings.Contains(ip, "/") {
			ipfromnet, err := convertcidr(ip)
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
			for _, hostfromnet := range ipfromnet {
				if !stringInSlice(hostfromnet, updscan.IPs) {
					updscan.IPs = append(updscan.IPs, hostfromnet)
				}
			}
		} else {
			if !stringInSlice(ip, updscan.IPs) {
				updscan.IPs = append(updscan.IPs, ip)
			}
		}
	}
	updscan.ScanRunning = scanrequest.ScanRunning
	query := bson.M{"_id": bson.ObjectIdHex(scanid)}
	dataupdate := bson.M{"$set": bson.M{"name": updscan.Name, "ips": updscan.IPs, "portstart": updscan.PortStart, "portend": updscan.PortEnd, "scanrunning": updscan.ScanRunning}}
	err := col.Update(query, dataupdate)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	return c.JSONPretty(http.StatusOK, updscan, "    ")
}

func newScan(c echo.Context) error {
	scanrequest := new(NewScan)
	var newscan scanner.Scan
	if err := c.Bind(scanrequest); err != nil {
		return err
	}
	newscan.BsonID = bson.NewObjectId()
	newscan.Name = scanrequest.Name
	newscan.PortStart = scanrequest.PortStart
	newscan.PortEnd = scanrequest.PortEnd
	wholeiplist := strings.Split(scanrequest.IPs, ",")
	for _, ip := range wholeiplist {
		if strings.Contains(ip, "/") {
			ipfromnet, err := convertcidr(ip)
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
			for _, hostfromnet := range ipfromnet {
				if !stringInSlice(hostfromnet, newscan.IPs) {
					newscan.IPs = append(newscan.IPs, hostfromnet)
				}
			}
		} else {
			if !stringInSlice(ip, newscan.IPs) {
				newscan.IPs = append(newscan.IPs, ip)
			}
		}
	}
	newscan.ScanRunning = false
	if newscan.Name != "" && newscan.IPs != nil && newscan.PortStart < newscan.PortEnd {
		col, session := mongoConn()
		defer session.Close()
		col.Insert(newscan)
		return c.JSONPretty(http.StatusOK, newscan, "    ")
	}
	return c.NoContent(http.StatusBadRequest)
}

func runScan(c echo.Context) error {
	scanid := c.Param("id")
	var scan scanner.Scan
	col, session := mongoConn()
	defer session.Close()
	err := col.Find(bson.M{"_id": bson.ObjectIdHex(scanid)}).One(&scan)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	if scan.ScanRunning {
		return c.NoContent(http.StatusNotAcceptable)
	}
	colQuerier := bson.M{"_id": scan.BsonID}
	change := bson.M{"$set": bson.M{"scanrunning": true}}
	err = col.Update(colQuerier, change)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	go scanner.ScanStart(scan, scanid)
	return c.JSONPretty(http.StatusOK, scan, "    ")
}

func getScanResult(c echo.Context) error {
	scanid := c.Param("id")
	resultid := c.Param("resultid")
	searchquery := c.QueryParam("query")
	var scan scanner.Scan
	col, session := mongoConn()
	defer session.Close()
	err := col.Find(bson.M{"_id": bson.ObjectIdHex(scanid), "results": resultid}).One(&scan)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	res := []scanner.ScanData{}
	res = nil
	client, ctx := elasticInit()
	id, err := uuid.Parse(resultid)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	res = elasticConn(ctx, id, searchquery, client)
	if res != nil {
		return c.JSONPretty(http.StatusOK, res, "    ")
	}
	return c.NoContent(http.StatusNotFound)
}

func reportScan(c echo.Context) error {
	var oldscaniplist []string
	var newscaniplist []string
	var appearedips []string
	var disappearedips []string
	var currentips []string
	var results []reporter.ResultData
	oldscanid := c.Param("os")
	newscanid := c.Param("ns")
	oldscandata := reporter.GetScanData(oldscanid)
	newscandata := reporter.GetScanData(newscanid)
	oldscaniplist = listips(oldscandata, oldscaniplist)
	newscaniplist = listips(newscandata, newscaniplist)
	for _, oldip := range oldscaniplist {
		if stringInSlice(oldip, newscaniplist) {
			currentips = append(currentips, oldip)
		} else if !stringInSlice(oldip, newscaniplist) {
			disappearedips = append(disappearedips, oldip)
		}
	}
	for _, newip := range newscaniplist {
		if stringInSlice(newip, oldscaniplist) && !stringInSlice(newip, currentips) {
			currentips = append(currentips, newip)
		} else if !stringInSlice(newip, newscaniplist) {
			appearedips = append(appearedips, newip)
		}
	}
	for _, curip := range currentips {
		var new_scan_ports []string
		var old_scan_ports []string
		result := reporter.ResultData{}
		result.OldScanDate = oldscandata[0].ScanDate
		result.NewScanDate = newscandata[0].ScanDate
		result.IP = curip
		for _, scandata := range append(oldscandata, newscandata...) {
			if scandata.ScanID.String() == newscanid && scandata.IP == curip {
				new_scan_ports = append(new_scan_ports, strconv.Itoa(scandata.Port))
				// result.ScanDate = scandata.ScanDate
			} else if scandata.ScanID.String() == oldscanid && scandata.IP == curip {
				old_scan_ports = append(old_scan_ports, strconv.Itoa(scandata.Port))
				// result.ScanDate = scandata.ScanDate
			}
		}
		for _, port := range new_scan_ports {
			portdata := reporter.PortData{}
			if stringInSlice(port, old_scan_ports) {
				// port was always here
				for _, oldlonescandata := range oldscandata {
					for _, newlonescandata := range newscandata {
						if (oldlonescandata.IP == newlonescandata.IP && oldlonescandata.Port == newlonescandata.Port) && ((strconv.Itoa(newlonescandata.Port) == port || strconv.Itoa(oldlonescandata.Port) == port) && (newlonescandata.IP == curip || oldlonescandata.IP == curip)) {
							portdata := newlonescandata.ComparePortCVEs(oldlonescandata, portdata)
							result.Ports = append(result.Ports, portdata)
						}
					}
				}
			} else if !stringInSlice(port, old_scan_ports) {
				// port is new
				for _, newlonescandata := range newscandata {
					if newlonescandata.IP == curip && port == strconv.Itoa(newlonescandata.Port) {
						var err error
						portdata.Port, err = strconv.Atoi(port)
						if err != nil {
							return c.NoContent(http.StatusBadRequest)
						}
						portdata.Cves = newlonescandata.Cves
						portdata.PortBannerNew = newlonescandata.PortBanner
						portdata.PortBannerNewSHA256 = newlonescandata.PortBannerSHA256
						portdata.PortBannerChanged = false
						portdata.PortIsNew = true
						result.Ports = append(result.Ports, portdata)
					}
				}
			}
		}
		for _, port := range old_scan_ports {
			portdata := reporter.PortData{}
			if !stringInSlice(port, new_scan_ports) {
				// port has disappeared
				for _, oldlonescandata := range oldscandata {
					if oldlonescandata.IP == curip && port == strconv.Itoa(oldlonescandata.Port) {
						var err error
						portdata.Port, err = strconv.Atoi(port)
						if err != nil {
							return c.NoContent(http.StatusBadRequest)
						}
						portdata.Cves = oldlonescandata.Cves
						portdata.PortBannerOld = oldlonescandata.PortBanner
						portdata.PortBannerOldSHA256 = oldlonescandata.PortBannerSHA256
						portdata.PortBannerChanged = false
						portdata.PortIsNew = false
						result.Ports = append(result.Ports, portdata)
					}
				}
			}
		}
		results = append(results, result)
	}
	return c.JSONPretty(http.StatusOK, results, "    ")
}

func listips(scandata []reporter.ReporterscanData, iplist []string) []string {
	for _, ipdata := range scandata {
		if !stringInSlice(ipdata.IP, iplist) {
			iplist = append(iplist, ipdata.IP)
		}
	}
	return iplist
}

/*
func scheduleScan(c echo.Context) error {
	scanid := c.Param("id")
	period := c.QueryParam("period")
	var task scheduler.Task
	tsk, err := task.New(scanid, period)
	if err != nil {
		return c.JSONPretty(http.StatusBadRequest, err, "    ")
	}
	return c.JSONPretty(http.StatusOK, tsk, "    ")
}
*/
// List of funcs for help and serializing
func mongoConn() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial(config.DataBaseServer.Host)
	if err != nil {
		log.Print(err)
	}
	session.SetMode(mgo.Monotonic, true)
	col := session.DB(config.DataBaseServer.DBName).C(config.DataBaseServer.ScansCol)
	return col, session
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func convertcidr(entry string) ([]string, error) {
	var ipfromcidr []string
	ip, ipnet, err := net.ParseCIDR(entry)
	if err != nil {
		return nil, err
	}
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		ipfromcidr = append(ipfromcidr, ip.String())
	}
	return ipfromcidr, nil
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func elasticInit() (*elastic.Client, context.Context) {
	ctx := context.Background()
	client, err := elastic.NewClient(elastic.SetURL("http://"+config.ElasticServer.Host+":"+config.ElasticServer.Port), elastic.SetScheme("http"), elastic.SetSniff(false))
	if err != nil {
		log.Fatal(err)
	}
	info, code, err := client.Ping("http://" + config.ElasticServer.Host + ":" + config.ElasticServer.Port).Do(ctx)
	log.Printf("Elasticsearch returned with code %d and version %s\n", code, info.Version.Number)
	return client, ctx
}
func elasticConn(ctx context.Context, id uuid.UUID, searchquery string, client *elastic.Client) []scanner.ScanData {
	var res []scanner.ScanData
	var myquery string
	myquery = "* AND "
	if searchquery != "" {
		myquery = searchquery + " AND "
	}
	searchResult, err := client.Search().
		Index("scans-*").
		Query(elastic.NewQueryStringQuery(myquery + "ScanID:" + id.String())).
		Size(10000).
		Pretty(true).
		Do(ctx)
	if err != nil {
		log.Fatal(err)
	}
	for _, hit := range searchResult.Hits.Hits {
		sres := scanner.ScanData{}
		err := json.Unmarshal(*hit.Source, &sres)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, sres)
	}
	return res
}
